var streamUrl = 'https://storage.googleapis.com/pubfront-files/hls-unencrypted-test/variant-playlist.m3u8';

// Create HTML element
var playerEl = document.createElement('audio');
document.body.appendChild(playerEl);

// Load HLS
if(Hls.isSupported()) {
  var hls = new Hls();
  hls.loadSource(streamUrl);
  hls.attachMedia(playerEl);
  hls.on(Hls.Events.MANIFEST_PARSED, function() {
      // Ready to play
  });
} else if (playerEl.canPlayType('application/vnd.apple.mpegurl')) {
  playerEl.src = streamUrl;
  playerEl.addEventListener('loadedmetadata', function() {
      // Ready to play
  });
}

// Initialize player
var player = new Plyr(playerEl);
var autoplay = localStorage.getItem('persistentPlayerPlaying') || '0';
var time = localStorage.getItem('persistentPlayerTime') || 'false';

player.on('play pause', function() {
  localStorage.setItem('persistentPlayerPlaying', String(player.playing));
});

function onFirstPlay() {
    console.log('WOOOO');
    if(time > 0) {
      player.currentTime = Number(time);
    }
    player.off('playing', onFirstPlay);
}
player.on('playing', onFirstPlay);

player.on('timeupdate', function() {
  var time = player.currentTime;
  if(time > 0) {
    console.log('timeupdate event', time);
    localStorage.setItem('persistentPlayerTime', player.currentTime);
  }
});

player.on('progress playing play pause timeupdate volumechange seeking seeked ratechange ended enterfullscreen exitfullscreen captionsenabled captionsdisabled languagechange controlshidden controlsshown ready loadstart loadeddata loadedmetadata qualitychange canplay canplaythrough stalled waiting emptied cuechange error', function(event) {
  console.log(event);
});

if(autoplay === 'true') {
  console.log('SHEEEE');
  player.play();
}
